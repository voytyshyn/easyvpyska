﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.WebUI.Abstract;
using EasyVpyska.Entities;
using EasyVpyska.WebUI.Frontend.Controllers;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;

namespace EasyVpyska.WebUI.Frontend.Test
{
    [TestClass]
    public class SearchTest
    {
        Mock<IImageService> _image = new Mock<IImageService>();
        Mock<IUserRepository> _userMock = new Mock<IUserRepository>();
        Mock<IAdRepository> _adMock = new Mock<IAdRepository>();    

        [TestMethod]
        public void SearchAdIndexTest()
        {                
            byte[] img = new byte[] { 0, 1, 5, 2 };

            List<Ad> ads= new List<Ad>();
            for (int i = 0; i < 6; i++)
            { 
                Ad ad=new Ad();
                ad.Id = i+1;
                AdAuthor author= new AdAuthor();
                author.Id = i+1;
                ad.Author = author;
                ad.IsEnabled=true;                           
                ads.Add(ad);            
            }

            _adMock.Setup(a => a.GetAds(It.IsAny<AdFilter>(), 1, 6)).Returns(ads);        
            _image.Setup(a => a.LoadPhoto(It.IsAny<int>())).Returns(new ProfileImage(){ File = img });         
            SearchAdController controller = new SearchAdController(_adMock.Object, _userMock.Object, _image.Object);
                       
            // Act
            ViewResult result = controller.Index() as ViewResult;
           
            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void SearchAdIndexImageTest()
        {    
            List<Ad> ads = new List<Ad>();
            for (int i = 0; i < 6; i++)
            {
                Ad ad = new Ad();
                ad.Id = i + 1;
                AdAuthor author = new AdAuthor();
                author.Id = i + 1;
                ad.Author = author;
                ad.IsEnabled = true;
                ads.Add(ad);
            }

            _adMock.Setup(a => a.GetAds(It.IsAny<AdFilter>(), 1, 6)).Returns(ads);
            _image.Setup(a => a.LoadPhoto(It.IsAny<int>())).Returns(new ProfileImage() { File = null });  
            SearchAdController controller = new SearchAdController(_adMock.Object, _userMock.Object, _image.Object);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }


           [TestMethod]
          public void FilterTest()
          {   
              byte[] img = new byte[] { 0, 1, 5, 2 };

               List<Ad> ads = new List<Ad>();
               for (int i = 0; i < 6; i++)
               {
                    Ad ad = new Ad();
                    ad.Id = i + 1;
                    AdAuthor author = new AdAuthor();
                    author.Id = i + 1;
                    ad.Author = author;
                    ad.IsEnabled = true;
                    ads.Add(ad);
               }
                             
              _adMock.Setup(a => a.GetAds(It.IsAny<AdFilter>(), 1, 6)).Returns(ads);
              _adMock.Setup(a => a.CountSuitableAds(It.IsAny<AdFilter>())).Returns(ads.Count);
              _image.Setup(a => a.LoadPhoto(It.IsAny<int>())).Returns(new ProfileImage() { File = img });  
              SearchAdController controller = new SearchAdController(_adMock.Object, _userMock.Object, _image.Object);

              // Act
              PartialViewResult result = controller.Filter("", "", "", "", 1, 6 ) as PartialViewResult;

              // Assert
              Assert.IsNotNull(result);
          }

           [TestMethod]
           public void FilterSecondTest()
           {
               List<Ad> ads = new List<Ad>();
               for (int i = 0; i < 6; i++)
               {
                   Ad ad = new Ad();
                   ad.Id = i + 1;
                   AdAuthor author = new AdAuthor();
                   author.Id = i + 1;
                   ad.Author = author;
                   ad.IsEnabled = true;
                   ads.Add(ad);
               }

               _adMock.Setup(a => a.GetAds(It.IsAny<AdFilter>(), 1, 6)).Returns(ads);
               _adMock.Setup(a => a.CountSuitableAds(It.IsAny<AdFilter>())).Returns(ads.Count);
               _image.Setup(a => a.LoadPhoto(It.IsAny<int>())).Returns(new ProfileImage() { File = null });  
               SearchAdController controller = new SearchAdController(_adMock.Object, _userMock.Object, _image.Object);

               // Act
               PartialViewResult result = controller.Filter("search", "Lviv", "22.08.2015", "23.08.2016", 1, 6) as PartialViewResult;
                
               // Assert
               Assert.IsNotNull(result);
           }

           [TestMethod]
           public void FilterThirdTest()
           {
               byte[] img = new byte[] { 0, 1, 5, 2 };

               List<Ad> ads = new List<Ad>();
               for (int i = 0; i < 6; i++)
               {
                   Ad ad = new Ad();
                   ad.Id = i + 1;
                   AdAuthor author = new AdAuthor();
                   author.Id = i + 1;
                   ad.Author = author;
                   ad.IsEnabled = true;
                   ads.Add(ad);
               }

               _adMock.Setup(a => a.GetAds(It.IsAny<AdFilter>(), 1, 6)).Returns(ads);               
               _adMock.Setup(a => a.CountSuitableAds(It.IsAny<AdFilter>())).Returns(ads.Count);
               _image.Setup(a => a.LoadPhoto(It.IsAny<int>())).Returns(new ProfileImage() { File = img });  
               SearchAdController controller = new SearchAdController(_adMock.Object, _userMock.Object, _image.Object);

               // Act
               PartialViewResult result = controller.Filter("offer", "", "", "", 1, 6) as PartialViewResult;

               // Assert
               Assert.IsNotNull(result);
           }
    }
}
