﻿
(function ($) {

    var $validator = $("#commentForm").validate({
        rules:
            {
                name: {
                    required: true,
                    maxlength: 16,
                    minlength: 3,
                    regex: /^[a-zA-Z]+$/,
                },
                surname: {
                    required: true,
                    maxlength: 16,
                    minlength: 3,
                    regex: /^[a-zA-Z]+$/,
                },
                date_of_birth: {
                    required: true,
                    date: true,
                },
                password: {
                    minlength: 8,
                    maxlength: 16,
                    regex: /^[a-zA-Z0-9]{8,16}$/
                },
                passwordRepeat: {   
                    minlength: 8,
                    maxlength: 16,
                    regex: /^[a-zA-Z0-9]{8,16}$/,
                    equalTo: "#password"
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "/Edit/IsEmailAwaliable",
                        timeout: 2000,
                        type: "post"
                    }
                },
                phone: {
                    required: true,
                    regex: /^\+[0-9]{12}$/,
                    remote: {
                        url: "/Edit/IsPhoneAwaliable",
                        timeout: 2000,
                        type: "post"
                    }
                },
                address: {
                    required: true,
                    maxlength: 64
                },
                desctiption: {
                    maxlength: 256
                }
                ,
                photo: {
                    filesize: 3000000
                }
            },
        messages:
        {
            password: {
                regex: "Enter a valid password."
            },
            passwordRepeat: {
                regex: "Enter a valid password"
            },
            email: {
                remote: "This email is already taken."
            },
            phone: {
                regex: "Enter valis phone number like +380000000000",
                remote: "This phone is already taken."
            },
            name: {
                regex: "Please use english letters only"
            },
            surname: {
                regex: "Please use english letters only"
            },
            country: {
                regex: "Please use english letters only"
            },
            city: {
                regex: "Please use english letters only"
            },
            photo: {
                filesize: "File must be less than 3MB"
            }
        }
    });

    $.validator.addMethod('filesize', function (value, element, param) {
        // param = size (en bytes) 
        // element = element to validate (<input>)
        // value = value of the element (file name)
        return this.optional(element) || (element.files[0].size <= param)
    });

    var editPage = function () {

        var that = this;
        var Autocomplete;
        var jcrop_api;

        var cropSize = {
            Width: 0,
            Height: 0,
            X: 0,
            Y: 0
        }

        var componentForm = {
            street_number: { type: 'short_name', field: '' },
            route: { type: 'long_name', field: '' },
            locality: { type: 'long_name', field: '' },
            country: { type: 'long_name', field: '' },
        };

        this.initialize = function () {

            

            console.log("[edit-page] initialize", arguments);

            this.$login = $("#login");
            this.$name = $("#name");
            this.$surname = $("#surname");
            this.$dateOfBirth = $("#date-of-birth");
            this.$password = $("#password");
            this.$passwordRepeat = $("#passwordRepeat");
            this.$email = $("#email");
            this.$phone = $("#phone");
            this.$country = $("#country");
            this.$city = $("#city");
            this.$address = $("#address");
            this.$description = $("#description");

            this.$photo = $('#photo');

            this.$btnSend = $("#send-btn");

            this.$btnSend.on('click', this.onSend);
            this.$photo.on('change', this.onFileChange);
        }

        this.autocompleteInitialize = function () {

            var addressInput = document.getElementById('address');

            addressAutocomplete = new google.maps.places.Autocomplete(addressInput);

            addressAutocomplete.addListener("place_changed", this.fillInAddress);
        }

        this.fillInAddress = function () {
            that.fillIt(this);
        };

        this.fillIt = function (autocomplete) {
            var place = autocomplete.getPlace();

            for (addressInputType in componentForm) {
                componentForm[addressInputType].field = "";
            }

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];

                for (addressInputType in componentForm) {
                    if (addressType === addressInputType) {
                        var val = place.address_components[i][componentForm[addressType].type];
                        componentForm[addressType].field = val;
                    }
                }
            }
        }

        this.onSend = function () {

            console.log("[edit-page] onSend", arguments, this);

            var valid = $("#commentForm").valid();

            var password = that.$password.val();
            var passwordRepeat = that.$passwordRepeat.val();
            if (valid) {
                var user = {
                    Login: that.$login.val(),
                    FirstName: that.$name.val(),
                    SurName: that.$surname.val(),
                    DateOfBirth: that.$dateOfBirth.val(),
                    Email: that.$email.val(),
                    Phone: that.$phone.val(),
                    Country: componentForm['country'].field,
                    Town: componentForm['locality'].field,
                    Address: componentForm['route'].field + ' ' + componentForm['street_number'].field,
                    About: that.$description.val()
                }

                var data = new FormData();

                data.append('userJson', JSON.stringify(user));
                console.log(JSON.stringify(user));
                data.append('password', password);
                data.append('passwordRepeat', passwordRepeat);
                
                if (that.$photo.get(0).files[0] != null){
                    data.append('photo', that.$photo.get(0).files[0]);
                } else {
                    data.append('photo', that.$photo.get(0).files[0]);
                }

                data.append('cropJson', JSON.stringify(cropSize));

                $.ajax({
                    url: "/Edit/Update",
                    type: "POST",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function (data) {
                    console.log("[edit-page] onSend - done", arguments, this);

                    if (data.userId != -1) {
                        window.location.replace("/User/Index/" + data.userId);
                    }
                    else {
                        $validator.showErrors(data.violations);
                    }
                });
            }
        };

        this.onFileChange = function () {
            console.log("[edit-page] onFileChange - done", arguments, this);
            readURL(this);
            that.initJcrop();
        };

        this.initJcrop = function () {
            $('#avatar').Jcrop({
                onChange: setCoordsAndImgSize,
                addClass: 'jcrop-centered',
                aspectRatio: 1,
                bgOpacity: .3,
                boxWidth: 450,
                boxHeight: 400
            }, function () {

                jcrop_api = this;

            });
        };

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#avatar').attr('src', e.target.result);
                    jcrop_api.setImage(e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        function setCoordsAndImgSize(e) {

            cropSize.Width = Math.round(e.w);
            cropSize.Height = Math.round(e.h);

            cropSize.X = Math.round(e.x);
            cropSize.Y = Math.round(e.y);
        }
    };

    $(document).on("ready", function () {
        console.log("[edit-page] has loaded", arguments, this);
        var page = new editPage();
        page.initialize();
        page.autocompleteInitialize();
    });

})(window.jQuery);