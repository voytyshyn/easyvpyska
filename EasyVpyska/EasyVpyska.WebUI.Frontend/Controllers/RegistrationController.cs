﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Drawing;

using EasyVpyska.Entities;
using EasyVpyska.WebUI.Abstract;
using EasyVpyska.Repositories.Abstract;

namespace EasyVpyska.WebUI.Frontend.Controllers
{
    public class RegistrationController : Controller
    {
        #region Fields
        private readonly IImageService _imageService;
        private readonly IUserRepository _userRepository;
        private readonly ISecurityManager _securityManager;
        #endregion

        #region Constructor
        public RegistrationController(ISecurityManager securityManager, IUserRepository userRepository,
            IImageService imageService)
        {
            _securityManager = securityManager;
            _userRepository = userRepository;
            _imageService = imageService;
        }
        #endregion

        #region Methods
        // GET: /Registration/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registrate(string userJson, string password, 
            string passwordRepeat, HttpPostedFileBase photo, string cropJson)
        {

            #region Validation

            if (userJson == null)
            {
                throw new ArgumentNullException("user", "can`t be null");
            } 

            #endregion

            int userId = -1;

            JavaScriptSerializer _jsonSerializer = new JavaScriptSerializer();
            var user = _jsonSerializer.Deserialize<User>(userJson);
            var crop = _jsonSerializer.Deserialize<ImageCrop>(cropJson);

            user.Status = true;
            user.Role = "user";

            var violations = CheckNewUserRules(user, password, passwordRepeat);
            
            if (violations.Count == 0)
            {
                userId = _securityManager.RegistrateUser(user, password); // Get exeption.
                _imageService.UploadPhoto(photo, userId, crop);
            }

            var data = new { userId = userId, violations = violations };

            var serializator = 
                new System.Web.Script.Serialization.JavaScriptSerializer();

            string dataJson = serializator.Serialize(data);

            if (userId != -1)
            {
                _securityManager.SetAuthCookie(user.Login, true);
            }

            return Content(dataJson, "application/json");
        }
        #endregion
        
        #region Helpers

        #region Validation helpers

        [HttpPost]
        public JsonResult IsLoginAwaliable(string login)
        {
            #region Validation
            if (login == null)
            {
                throw new ArgumentNullException();
            }
            #endregion

            if (_userRepository.IsLoginExist(login))
            {
                return new JsonResult { Data = false };
            }

            return new JsonResult { Data = true };
        }

        [HttpPost]
        public JsonResult IsEmailAwaliable(string email)
        {
            #region Validation
            if (email == null)
            {
                throw new ArgumentNullException();
            }
            #endregion

            if (_userRepository.IsEmailExist(email))
            {
                return new JsonResult { Data = false };
            }

            return new JsonResult { Data = true };
        }

        [HttpPost]
        public JsonResult IsPhoneAwaliable(string phone)
        {
            #region Validation
            if (phone == null)
            {
                throw new ArgumentNullException();
            }
            #endregion

            if (_userRepository.IsPhoneExist(phone))
            {
                return new JsonResult { Data = false };
            }

            return new JsonResult { Data = true };
        }

        private IDictionary<string, string> CheckNewUserRules(User user,
            string password, string passwordRepeat)
        {
            IDictionary<string, string> violations = new Dictionary<string, string>();

            CkeckLowercaseRules(user.Login, "login", violations);
            CheckWordRules(user.Login, "login", violations, maxLength: 16);
            CheckWordRules(user.FirstName, "name", violations);
            CheckWordRules(user.SurName, "surname", violations);

            CheckAddressRules(user, "address", violations);

            CheckEmailRules(user.Email, "email", violations);
            CheckPhoneRules(user.Phone, "phone", violations);

            CheckDateRules(user.DateOfBirth, "date_of_birth", violations);

            CheckPasswordRules(password, "password", violations, 8, 16);
            CheckPasswordRules(passwordRepeat, "passwordRepeat", violations, 8, 16);

            CheckPasswordSimilarity(password, passwordRepeat, "passwordRepeat",
                violations);

            CheckTextLength(user.About, "description", violations, 256);
            CheckTextLength(user.Town, "address", violations, 256);
            CheckTextLength(user.Country, "address", violations, 256);

            return violations;
        }
        private void CheckAddressRules(User user, string fieldName,
            IDictionary<string, string> violations)
        {
            var regex = new Regex("^[a-zA-Z]+$");

            if (string.IsNullOrEmpty(user.Country)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter country name please."));
            }

            if (string.IsNullOrEmpty(user.Town)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter town name please."));
            }
        }


        private void CheckImageRules(ProfileImage image, string fieldName, IDictionary<string, string> violations)
        {
            if (image != null
                && image.File.Length >= 3000000)
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please use smaller image"));
            }
        }

        private void CkeckLowercaseRules(string login, string fieldName,
            IDictionary<string, string> violations)
        {
            var regex = new Regex("^[a-z]+$");

            if (login != null
            && !regex.IsMatch(login)
            && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please use just english letters."));
            }
        }

        private void CheckWordRules(string word, string fieldName,
            IDictionary<string, string> violations,
            int minLength = 3,
            int maxLength = 64)
        {
            var regex = new Regex("^[a-zA-Z]+$");

            if (string.IsNullOrEmpty(word)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter value please."));
            }
            if (word != null
                && word.Length < minLength
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please enter at least " + minLength.ToString() +
                    " 3 characters."));
            }
            if (word != null
                && word.Length > maxLength
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please enter no more than " + maxLength.ToString() +
                    " characters."));
            }
            if (word != null
                && !regex.IsMatch(word)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please use just english letters."));
            }
        }

        private void CheckEmailRules(string email, string fieldName,
            IDictionary<string, string> violations)
        {
            var regex = new Regex("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+"
                + "(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

            if (string.IsNullOrEmpty(email)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter value please."));
            }
            if (email != null
                && !regex.IsMatch(email)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please enter a valid email address."));
            }
        }

        private void CheckPasswordSimilarity(string password,
            string passwordRepeat,
            string fieldName,
            IDictionary<string, string> violations)
        {
            if (password != null
                && passwordRepeat != null
                && !string.Equals(password, passwordRepeat)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please enter the same value again."));
            }
        }

        private void CheckPasswordRules(string password, string fieldName,
            IDictionary<string, string> violations,
            int minLength,
            int maxLength)
        {
            var regex = new Regex("^[a-zA-Z0-9]{8,16}$");

            if (string.IsNullOrEmpty(password)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter value please."));
            }
            if (password != null
                && !regex.IsMatch(password)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter a valid password"));
            }
            if (password != null
                && password.Length < minLength
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please enter at least " + minLength.ToString() +
                    " 3 characters."));
            }
            if (password != null
                && password.Length > maxLength
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please enter no more than " + maxLength.ToString() +
                    " characters."));
            }
        }

        private void CheckDateRules(DateTime dateOfBirth, string fieldName,
            IDictionary<string, string> violations
            )
        {
            if (dateOfBirth == null
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter value please."));
            }
            if (dateOfBirth != null
                && GetAge(dateOfBirth) >= 130
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter correct date. You should be younger"));
            }
            if (dateOfBirth != null
                && GetAge(dateOfBirth) < 5
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter correct date. You should be older"));
            }
        }

        private void CheckTextLength(string about, string fieldName,
            IDictionary<string, string> violations,
            int maxLength)
        {
            if (about != null
                && about.Length >= maxLength
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                     "Please enter no more than " + maxLength.ToString() +
                    " characters."));
            }
        }

        private void CheckPhoneRules(string phone, string fieldName,
            IDictionary<string, string> violations)
        {
            var regex = new Regex("^\\+[0-9]{12}$");

            if (string.IsNullOrEmpty(phone)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter value please."));
            }

            bool res = regex.IsMatch(phone);
            if (phone != null
                && phone != ""
                && !regex.IsMatch(phone)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter valid phone number like +380000000000"));
            }
        }

        private int GetAge(DateTime dateOfBirth)
        {
            return (DateTime.Today.Year - dateOfBirth.Year);
        } 

        #endregion

        #endregion
	}
}