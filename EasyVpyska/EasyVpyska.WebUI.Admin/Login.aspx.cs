﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Configuration;
using Ninject;


using EasyVpyska.WebUI;
using EasyVpyska.WebUI.Abstract;
using EasyVpyska.WebUI.Admin.Code;

namespace EasyVpyska.WebUI.Admin
{
    public partial class LoginPage : System.Web.UI.Page
    {
        #region Fields

        private ISecurityManager _securityManager; 

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            _securityManager = ServiceLocator.Kernel.Get<ISecurityManager>();

            if (_securityManager.IsCurrentUserAuthenticated())
            {
                if (_securityManager.IsUserInRole(User.Identity.Name, "admin"))
                {
                    _securityManager.RedirectFromLoginPage(User.Identity.Name, true);
                }
                else
                {
                    LegendStatus.Text = "Access deny. Try another account";
                }
            }

        }

        protected void btnLogIn_Click(object sender, EventArgs e)
        {
            Page.Validate();

            if (!Page.IsValid)
            {
                return;
            }

            if (_securityManager.Authentication(txtLogin.Text, txtPassword.Text))
            {
                _securityManager.RedirectFromLoginPage(txtLogin.Text, true);
            }
            else
            {
                LegendStatus.Text = "Invalid username or password!";
            }
        }
    }
}