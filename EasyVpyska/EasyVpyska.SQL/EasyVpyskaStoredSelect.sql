USE [EasyVpyska];

GO

CREATE PROCEDURE uspGetUsers
	@Filter varchar(128),
	@Status bit,
	@startIndex int,
	@maxRows int,
	@TotalRecord int output
AS

	SELECT @TotalRecord = Count(*) from tblUser

	SELECT RowNo,
			Id, 
			Login, 
			Firstname,
			Surname, 
			DateOfBirth,	
			Email, 
			Phone, 
			Country, 
			Town, 
			Address, 
			Status, 
			AboutMyself
	FROM
	(
		SELECT Row_number() over(Order by Id ASC) as RowNo,
			Id, 
			Login, 
			Firstname, 
			Surname, 
			DateOfBirth, 
			Email, 
			Phone, 
			Country, 
			Town, 
			Address, 
			Status, 
			AboutMyself

		FROM tblUser
		WHERE
			( 
			Login LIKE '%' + @Filter + '%'
			OR Firstname LIKE '%' + @Filter + '%'
			OR Surname LIKE '%' + @Filter + '%'
			OR AboutMyself LIKE '%' + @Filter + '%'
			OR Country LIKE '%' + @Filter + '%'
			OR Town LIKE '%' + @Filter + '%'
			OR Address LIKE '%' + @Filter + '%'
			OR Email LIKE '%' + @Filter + '%'
			)
			AND 
			(
				Status = @Status 
				OR @Status IS NULL
			)
	) AS Tab
	WHERE Tab.RowNo BETWEEN (@startIndex+1) AND ((@startIndex+1)*@maxRows)

	ORDER BY
		Id ASC 
		  
GO

CREATE PROCEDURE uspGetUsersCount 
	@Filter varchar(128),
	@Status bit
AS

	SELECT COUNT(*)
	FROM [tblUser]
	WHERE 
		(
		Login			LIKE '%' + @Filter + '%'
		OR Firstname	LIKE '%' + @Filter + '%'
		OR Surname		LIKE '%' + @Filter + '%'
		OR AboutMyself	LIKE '%' + @Filter + '%'
		OR Country		LIKE '%' + @Filter + '%'
		OR Town			LIKE '%' + @Filter + '%'
		OR Address		LIKE '%' + @Filter + '%'
		OR Email		LIKE '%' + @Filter + '%'
		)
		AND 
		(
		Status = @Status 
		OR @Status IS NULL
		);

GO

CREATE PROCEDURE uspGetUser
	@Login varchar(256)

AS

	SELECT 
			u.Id, 
			u.Login,
			r.Role,
			u.Firstname,
			u.Surname, 
			u.DateOfBirth,	
			u.Email, 
			u.Phone, 
			u.Country, 
			u.Town, 
			u.Address, 
			u.Status, 
			u.AboutMyself
	FROM [dbo].[tblUser] u LEFT JOIN [dbo].[tblRole] r
	ON
		u.Role = r.Id
	WHERE 
		u.Login = @Login
		
GO

CREATE PROCEDURE uspGetUserById
	@Id int

AS

	SELECT 
			u.Id, 
			u.Login,
			r.Role,
			u.Firstname,
			u.Surname, 
			u.DateOfBirth,	
			u.Email, 
			u.Phone, 
			u.Country, 
			u.Town, 
			u.Address, 
			u.Status, 
			u.AboutMyself
	FROM [dbo].[tblUser] u LEFT JOIN [dbo].[tblRole] r
	ON
		u.Role = r.Id
	WHERE 
		u.Id = @Id;
		
 GO
 
 CREATE PROCEDURE uspGetAdAuthorById
	@Id int

AS

	SELECT 
			u.Id, 
			r.Role,
			u.Firstname,
			u.Surname, 
			u.DateOfBirth,	
			u.Email, 
			u.Town, 
			u.Status
	FROM [dbo].[tblUser] u LEFT JOIN [dbo].[tblRole] r
	ON
		u.Role = r.Id
	WHERE 
		u.Id = @Id;
		
GO

CREATE PROCEDURE uspGetAdsByFilter
	@IsRequest bit,
	@Town NVARCHAR(64),
	@Start_Time DATETIME,
	@End_Time DATETIME
	
AS

	SELECT 
	Id,
	IsRequest,
	IsEnabled,
	Title,
	AuthorId,
	Description,
	Begin_date,
	End_date,
	Country	,
	Town,
	Address

	FROM tblAd 
	WHERE
		( @IsRequest IS NULL OR IsRequest = @IsRequest )
		AND ( @Start_Time IS NULL OR End_date >= @Start_Time ) 
		AND ( @End_Time IS NULL OR Begin_date <= @End_Time )
		AND ( @Town IS NULL OR Town = @Town );
GO

CREATE PROCEDURE uspGetCommentsByAdId	
	@Id int
AS
	SELECT 
		tblAdComment.Id,
		tblAdComment.IsEnabled,
		tblAdComment.AdId,
		tblAdComment.UserId,
		tblUser.Firstname,
		tblAdComment.[Time],
		tblAdComment.[Comment]
		
	FROM [dbo].[tblAdComment]
	LEFT JOIN [dbo].[tblUser]
	ON tblAdComment.UserId=tblUser.Id
	WHERE 
		AdId = @Id

	ORDER BY [Time] DESC;
		
		
GO

CREATE PROCEDURE uspGetAllComments	
	@Id int
AS
	SELECT 
		tblAdComment.Id,
		tblAdComment.IsEnabled,
		tblAdComment.AdId,
		tblAdComment.UserId,
		tblUser.Firstname,
		tblAdComment.[Time],
		tblAdComment.[Comment]
		
	FROM [dbo].[tblAdComment]
	LEFT JOIN [dbo].[tblUser]
	ON tblAdComment.UserId=tblUser.Id

	ORDER BY [Time] DESC;
		
		
GO


CREATE PROCEDURE uspGetAds
	@IsRequest bit,
	@IsEnabled bit,
	@PageIndex int,
	@AdsPerPage int,
	@AuthorId int,
	@Town NVARCHAR(64),
	@Start_Time DATETIME,
	@End_Time DATETIME,
	@Active bit
	
AS

	WITH SuitableAds AS
(
	SELECT 
	tblAd.Id,
	tblAd.IsRequest,
	tblAd.IsEnabled,
	tblAd.Title,
	tblAd.Description,
	tblAd.Begin_date,
	tblAd.End_date,
	tblAd.Country,
	tblAd.Town,
	tblAd.Address,
	tblAd.IsActive,
	tblUser.Id AS AuthorId,
	tblUser.Firstname AS AuthorName,
	tblUser.Surname AS AuthorSurname,
	tblUser.Email AS AuthorEmail,
	tblUser.Town AS AuthorTown,
	tblUser.Status AS AuthorStatus,
	tblUser.DateOfBirth AS AuthorBirth,
	ROW_NUMBER() OVER (ORDER BY tblAd.Id DESC)  AS RowNumber

	FROM tblAd INNER JOIN tblUser
	ON tblAd.AuthorId = tblUser.Id
	WHERE
		( @AuthorId IS NULL OR AuthorId = @AuthorId )
		AND	( @IsRequest IS NULL OR IsRequest = @IsRequest )
		AND ( @IsEnabled IS NULL OR IsEnabled = @IsEnabled )
		AND ( @Start_Time IS NULL OR End_date >= @Start_Time ) 
		AND ( @End_Time IS NULL OR Begin_date <= @End_Time )		
		AND ( @Town IS NULL OR tblAd.Town = @Town )
		AND ( @Active IS NULL OR tblAd.IsActive = @Active )
)
	SELECT 
	Id,
	IsRequest,
	IsEnabled,
	Title,
	Description,
	Begin_date,
	End_date,
	Country,
	Town,
	Address,
	IsActive,
	AuthorId,
	AuthorName,
	AuthorSurname,
	AuthorEmail,
	AuthorTown,
	AuthorStatus,
	AuthorBirth
	
	FROM SuitableAds
	WHERE RowNumber BETWEEN (@PageIndex - 1) * @AdsPerPage + 1 AND (@PageIndex * @AdsPerPage)

GO
	
CREATE PROCEDURE uspCountSuitableAds
	@IsRequest bit,
	@IsEnabled bit,
	@AuthorId int,
	@Town NVARCHAR(64),
	@Start_Time DATETIME,
	@End_Time DATETIME,
	@Active bit

AS

	WITH SuitableAds AS
(
	SELECT 

	Id,
	IsRequest,
	IsEnabled,
	Title,
	AuthorId,
	Description,
	Begin_date,
	End_date,
	Country,
	Town,
	Address,
	ROW_NUMBER() OVER (ORDER BY Id DESC)  AS RowNumber
	FROM tblAd 
	WHERE
		( @AuthorId IS NULL OR AuthorId = @AuthorId ) 
		AND	( @IsRequest IS NULL OR IsRequest = @IsRequest )
		AND ( @IsEnabled IS NULL OR IsEnabled = @IsEnabled )
		AND ( @Start_Time IS NULL OR End_date >= @Start_Time ) 
		AND ( @End_Time IS NULL OR Begin_date <= @End_Time )
		AND ( @Town IS NULL OR Town = @Town )
		AND ( @Active IS NULL OR tblAd.IsActive = @Active )
)
	SELECT COUNT (Id) FROM SuitableAds;

GO

CREATE PROCEDURE uspGetAdById
	@Id int
AS
	SELECT 
	tblAd.Id,
	tblAd.IsRequest,
	tblAd.IsEnabled,
	tblAd.Title,
	tblAd.Description,
	tblAd.Begin_date,
	tblAd.End_date,
	tblAd.Country,
	tblAd.Town,
	tblAd.Address,
	tblAd.IsActive,
	tblUser.Id AS AuthorId,
	tblUser.Firstname,
	tblUser.Surname,
	tblUser.Email,
	tblUser.Town AS AuthorTown,
	tblUser.Status,
	tblUser.DateOfBirth
	
	FROM tblAd INNER JOIN tblUser
	ON tblAd.AuthorId = tblUser.Id
	WHERE tblAd.Id = @Id;

	GO

		
CREATE PROCEDURE uspUpdateAdEnableStatus
	@Id int,
	@Enable bit
AS
UPDATE tblAd
SET tblAd.IsEnabled =  @Enable
WHERE tblAd.Id = @Id;

GO


CREATE PROCEDURE uspUpdateAdActiveStatus
	@Id int,
	@Active bit
AS
UPDATE tblAd
SET tblAd.IsActive =  @Active
WHERE tblAd.Id = @Id;

GO

CREATE PROCEDURE uspUpdateAdCommentStatus
	@Id int,
	@Status bit
AS
UPDATE tblAdComment
SET tblAdComment.IsEnabled = @Status
WHERE tblAdComment.Id = @Id;

GO

CREATE PROCEDURE uspCountAdViewers
	@AdViewingId int	
AS
SELECT COUNT(AdId) AS AdViewing FROM tblAdViewers
WHERE AdId=@AdViewingId

GO

CREATE PROCEDURE uspUpdateUser
	@Id		      INT,
	@Password     VARCHAR(64),
	@Firstname    NVARCHAR(64), 
	@Surname      NVARCHAR(64),
	@DateOfBirth  DATETIME, 
	@Email        NVARCHAR(256),
	@Phone        NVARCHAR(64), 
	@Country      NVARCHAR(64),
	@Town         NVARCHAR(64),
	@Address      NVARCHAR(64),
	@AboutMyself  NVARCHAR(256)

AS

	IF @Password IS NOT NULL
		UPDATE tblUser SET Password = @Password WHERE Id = @Id;
	IF @Firstname IS NOT NULL
		UPDATE tblUser SET Firstname = @Firstname WHERE Id = @Id;
	IF @Surname IS NOT NULL
		UPDATE tblUser SET Surname = @Surname WHERE Id = @Id;
	IF @DateOfBirth IS NOT NULL
		UPDATE tblUser SET DateOfBirth = @DateOfBirth WHERE Id = @Id;
	IF @Email IS NOT NULL
		UPDATE tblUser SET Email = @Email WHERE Id = @Id;
	IF @Phone IS NOT NULL
		UPDATE tblUser SET Phone = @Phone WHERE Id = @Id;
	IF @Country IS NOT NULL
		UPDATE tblUser SET Country = @Country WHERE Id = @Id;
	IF @Town IS NOT NULL
		UPDATE tblUser SET Town = @Town WHERE Id = @Id;
	IF @Address IS NOT NULL
		UPDATE tblUser SET Address = @Address WHERE Id = @Id;
	IF @AboutMyself IS NOT NULL
		UPDATE tblUser SET AboutMyself = @AboutMyself WHERE Id = @Id;

GO

CREATE PROCEDURE uspUpdateImage
	@UserId INT,
	@FileName NVARCHAR(128),
	@Image  VARBINARY(MAX)
AS
	IF @UserId IS NOT NULL
		UPDATE tblImage SET [FileName] = @FileName, Image = @Image WHERE UserId = @UserId;

GO

CREATE PROCEDURE uspGetSubscribersByAdId	
	@Id int
AS
	SELECT 
		tblAdSubscribers.Id,
		tblAdSubscribers.AdId,
		tblAdSubscribers.UserId,
		tblUser.Firstname As Name,
		tblAdSubscribers.[Date]		
		
	FROM [dbo].[tblAdSubscribers]
	LEFT JOIN [dbo].[tblUser]
	ON tblAdSubscribers.UserId=tblUser.Id
	WHERE 
		AdId = @Id

	ORDER BY [Date] DESC;
		
 GO

