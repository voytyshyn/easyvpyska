﻿(function ($) {

    var $validator = $("#loginForm").validate();

    var LoginPage = function () {

        var that = this;

        this.initialize = function () {
            console.log("[logIn-partitial view] initialize", arguments);


            this.$txtName = $("#login");
            this.$txtPassword = $("#password");

            this.$btnSignIn = $("#btnSignIn");
            this.$btnSignUp = $("#btnSignUp");

            this.$btnSignIn.on("click", this.onSingIn);
            this.$btnSignUp.on("click", this.onSignUp);
        };

        this.onSingIn = function () {

            console.log("[logIn-partitial view] onSingIn", arguments, this);

            var name = that.$txtName.val();
            var password = that.$txtPassword.val();

            var valid = $("#loginForm").valid();

            $.ajax({
                url: "/LogIn/LogIn",
                type: "POST",
                dataType: "json",
                data: {
                    name: name,
                    password: password
                }
            }).done(function (data) {
                console.log("[logIn-partitial view] onSingIn - done", arguments, this);

                if (data.blocked || !data.authenticated || (data.userId == -1)) {
                    $validator.showErrors(data.violations);
                    console.log("[logIn-partitial view] onSingIn - non authenticated", arguments, this);
                }
                else {
                    window.location.replace("/User/Index/" + data.userId);
                }

            });
        };

        this.onSignUp = function () {
            console.log("[logIn-partitial view] onSignUp", arguments, this);

            window.location.replace("/Registration/Index");
        }
    };

    $(document).on("ready", function () {
        console.log("[logIn-partitial] has loaded", arguments, this);
        var page = new LoginPage();
        page.initialize();
    });

})(window.jQuery);