﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using EasyVpyska.Entities;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.Repositories.Sql;
using EasyVpyska.WebUI;
using EasyVpyska.WebUI.Abstract;

namespace EasyVpyska.WebUI.Admin
{
    public class EasyVpyskaConfigModule : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ConnectToSql"].ConnectionString;

            Bind<IUserRepository>().To<UserRepository>().WithConstructorArgument(connectionString);
            Bind<IAdRepository>().To<AdRepository>().WithConstructorArgument(connectionString);
            Bind<ISecurityManager>().To<SecurityManager>();
        }
    }
}