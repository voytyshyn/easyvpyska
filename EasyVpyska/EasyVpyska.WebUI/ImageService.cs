﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using EasyVpyska.Entities;
using EasyVpyska.WebUI.Abstract;
using EasyVpyska.Repositories.Abstract;

namespace EasyVpyska.WebUI
{
    public class ImageService : IImageService
    {
        #region Fields

        private IUserRepository _userRepository;

        #endregion

        #region Constructors

        public ImageService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        } 

        #endregion

        #region Methods

        public int UploadPhoto(HttpPostedFileBase photo, int userId, ImageCrop crop)
        {
            if (photo == null)
            {
                return -1;
            }

            var image = ProcessImage(photo, userId, crop);

            return _userRepository.UploadPhoto(image);
        }

        public ProfileImage LoadPhoto(int userId)
        {
            return _userRepository.LoadPhoto(userId);
        }

        public int UpdatePhoto(HttpPostedFileBase photo, int userId, ImageCrop crop)
        {
            if (photo == null)
            {
                return -1;
            }

            var image = ProcessImage(photo, userId, crop);

            if (_userRepository.IsProfilePhotoExists(image.UserId))
            {
                return _userRepository.UpdatePhoto(image);
            }
            else
            {
                return _userRepository.UploadPhoto(image);
            }
        }
        
        #endregion

        #region Helpers

        private ProfileImage ProcessImage(HttpPostedFileBase photo, int userId, ImageCrop crop)
        {
            if (photo == null)
            {
                throw new ArgumentNullException();
            }

            MemoryStream target = new MemoryStream();
            photo.InputStream.CopyTo(target);
            byte[] file = target.ToArray();

            var image = new ProfileImage { FileName = photo.FileName, File = file, UserId = userId };

            image.File = Crop(image.File, crop);

            return image;
        }

        private byte[] Crop(byte[] image, ImageCrop crop)
        {
            ImageConverter ic = new ImageConverter();
            Image img = (Image)ic.ConvertFrom(image);

            Bitmap bmap = (Bitmap)img.Clone();

            if ((crop.Height + crop.Y) > img.Height)
            {
                crop.Height = img.Height - crop.Y;
            }

            if ((crop.Width + crop.X) > img.Width)
            {
                crop.Width = img.Width - crop.X;
            }

            if ((crop.Height == 0) || (crop.Width == 0))
            {
                crop.X = 0;
                crop.Y = 0;

                if (img.Width <= img.Height)
                {
                    crop.Width = img.Width;
                    crop.Height = img.Width;
                }
                else
                {
                    crop.Width = img.Height;
                    crop.Height = img.Height;
                }
            }


            Rectangle rect = new Rectangle(crop.X, crop.Y,
                crop.Width, crop.Height);

            bmap = (Bitmap)bmap.Clone(rect, bmap.PixelFormat);

            return (byte[])ic.ConvertTo(bmap, typeof(byte[]));
        } 

        #endregion
    }
}
