﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.WebUI.Abstract;
using EasyVpyska.Entities;
using EasyVpyska.WebUI.Frontend.Controllers;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;
using System.IO;

namespace EasyVpyska.WebUI.Frontend.Test
{
    [TestClass]
    public class EditTest
    {
        Mock<IUserRepository> userMock = new Mock<IUserRepository>();
        Mock<ISecurityManager> security = new Mock<ISecurityManager>();
        Mock<IImageService> image = new Mock<IImageService>();

        [TestMethod]
        public void EditIndexTest()
        {    
            byte[] img = new byte[] { 0, 1, 5, 2 };

            userMock.Setup(a => a.GetUserById(It.Is<int>(i => i > 0))).Returns(new User() { Id = 1 });
            userMock.Setup(a => a.LoadPhoto(It.Is<int>(i => i > 0))).Returns(new EasyVpyska.Entities.ProfileImage() { File = img });
            security.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
            EditController controller = new EditController(userMock.Object, security.Object, image.Object);

            // Act
            ViewResult result = controller.Index("1") as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void EditIndexImgTest()
        {           
            User userTest = new User();
            userTest.Id = 1;

            userMock.Setup(a => a.GetUserById(It.Is<int>(i => i > 0))).Returns(userTest);
            userMock.Setup(a => a.LoadPhoto(It.Is<int>(i => i > 0))).Returns(new EasyVpyska.Entities.ProfileImage() { File = null });
            security.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
            EditController controller = new EditController(userMock.Object, security.Object, image.Object);

            // Act
            ViewResult result = controller.Index("1") as ViewResult;

            // Assert
            Assert.AreEqual(userTest, result.ViewData["User"]);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void EditIndexErrorTest()
        {
            EditController controller = new EditController(userMock.Object, security.Object, image.Object);
            
            // Act
            ViewResult result = controller.Index("abc") as ViewResult;
        }

        [TestMethod]
        public void UpdateTest()
        {
            var photo = new Mock<HttpPostedFileBase>();
            var stream = new Mock<Stream>();       
          
            photo.Setup(a => a.InputStream).Returns(stream.Object);
            User userTest = new User();
            userTest.Id = 1;
            string userJson = "{\"FirstName\":\"Petr\",\"SurName\":\"Voycikhovsky\",\"DateOfBirth\":\"01/13/1958\",\"Email\":\"voycikhovsky@mail.ru\",\"Phone\":\"+380995156814\",\"Country\":\"Ukraine\",\"Town\":\"Vinnica\",\"Address\":\"Наливайченка 3\",\"About\":\"Не хроплю!\"}";
            
            security.Setup(a => a.GetCurrentUserId()).Returns(1);
            userMock.Setup(a => a.UpdateUser(userTest, It.IsAny<string>())).Returns(1);
            userMock.Setup(a => a.LoadPhoto(It.Is<int>(i => i > 0))).Returns(new EasyVpyska.Entities.ProfileImage() { File = null });

            EditController controller = new EditController(userMock.Object, security.Object, image.Object);


            // Act
            var result = controller.Update(userJson, "12345678", "12345678", photo.Object, "") as ContentResult;

            // Assert           
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void UpdateFailedTest()
        {
            byte[] img = new byte[] { 0, 1, 5, 2 };
            var photo = new Mock<HttpPostedFileBase>();
            var stream = new Mock<Stream>();       
            photo.Setup(a => a.InputStream).Returns(stream.Object);
            User userTest = new User();
            userTest.Id = 1;
            string userJson = "{\"FirstName\":\"\",\"SurName\":\"\",\"DateOfBirth\":\"\",\"Email\":\"\",\"Phone\":\"\",\"Country\":\"\",\"Town\":\"\",\"Address\":\"\",\"About\":\"\"}";
          
            userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(new User());
            security.Setup(a => a.GetCurrentUserId()).Returns(1);
            userMock.Setup(a => a.UpdateUser(userTest, It.IsAny<string>())).Returns(1);
            userMock.Setup(a => a.LoadPhoto(It.Is<int>(i => i > 0))).Returns(new EasyVpyska.Entities.ProfileImage() { File = img });

            EditController controller = new EditController(userMock.Object, security.Object, image.Object);


            // Act
            var result = controller.Update(userJson, "14", "12341234123412341234", photo.Object, "") as ContentResult;

            // Assert           
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void UpdateFailedTownTest()
        {
            byte[] img = new byte[] { 0, 1, 5, 2 };
            var photo = new Mock<HttpPostedFileBase>();      
            var stub = new Mock<Stream>();
            stub.SetupGet(x => x.Length).Returns(4000000);

            photo.Setup(a => a.InputStream).Returns(stub.Object);

            var l = photo.Object.InputStream.Length;
            User userTest = new User();
            userTest.Id = 1;
            string userJson = "{\"FirstName\":\"\",\"SurName\":\"\",\"DateOfBirth\":\"01/13/2013\",\"Email\":\"\",\"Phone\":\"\",\"Country\":\"Ukraine\",\"Town\":\"\",\"Address\":\"\",\"About\":\"\"}";

            security.Setup(a => a.GetCurrentUserId()).Returns(1);
            userMock.Setup(a => a.UpdateUser(userTest, It.IsAny<string>())).Returns(1);
            userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(new User());
            userMock.Setup(a => a.LoadPhoto(It.Is<int>(i => i > 0))).Returns(new EasyVpyska.Entities.ProfileImage() { File = img });

            EditController controller = new EditController(userMock.Object, security.Object, image.Object);


            // Act
            var result = controller.Update(userJson, "12345678;", "12;;;;;34", photo.Object, "") as ContentResult;

            // Assert           
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void UpdateValidationTest()
        {
            var photo = new Mock<HttpPostedFileBase>();
            var stream = new Mock<Stream>();
            
            photo.Setup(a => a.InputStream).Returns(stream.Object);
            User userTest = new User();
            userTest.Id = 1;
            string userJson = "{\"FirstName\":\"Pe\",\"SurName\":\"VVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVoVo"+
                "o\",\"DateOfBirth\":\"01/13/1800\",\"Email\":\"voyciru\",\"Phone\":\"95156814\",\"Country\":\"" +
                "UkraUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkraineUkrainekraineUkraineUkraineine"+
                "\",\"Town\":\"Vinnica\",\"Address\":\"Наливайченка 3\",\"About\":\"Не хроплю!\"}";

            security.Setup(a => a.GetCurrentUserId()).Returns(1);
            userMock.Setup(a => a.UpdateUser(userTest, It.IsAny<string>())).Returns(1);
            userMock.Setup(a => a.LoadPhoto(It.Is<int>(i => i > 0))).Returns(new EasyVpyska.Entities.ProfileImage() { File = null });

            EditController controller = new EditController(userMock.Object, security.Object, image.Object);


            // Act
            var result = controller.Update(userJson, "12345679", "12345678", null, "") as ContentResult;

            // Assert           
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UpdateErrorTest()
        {
            var photo = new Mock<HttpPostedFileBase>();
            EditController controller = new EditController(userMock.Object, security.Object, image.Object);

            // Act
            var result = controller.Update(null, "12345678", "12345678", photo.Object, "") as ContentResult;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IsEmailAwaliableErrorTest()
        {
            EditController controller = new EditController(userMock.Object, security.Object, image.Object);

            // Act
            var result = controller.IsEmailAwaliable(null) as JsonResult;          
        }

        [TestMethod]
        public void IsEmailAwaliableTest()
        {           
            User user= new User();
            user.Email = "email";
            userMock.Setup(a => a.GetUserById( It.IsAny<int>())).Returns(user);            

            EditController controller = new EditController(userMock.Object, security.Object, image.Object);
            
            // Act
            var result = controller.IsEmailAwaliable("email") as JsonResult;

            // Assert           
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void IsEmailAwaliableSecondTest()
        {
            User user = new User();
            user.Email = "14";
            userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(user);            
            userMock.Setup(a => a.IsEmailExist(It.IsAny<string>())).Returns(true);

            EditController controller = new EditController(userMock.Object, security.Object, image.Object);

            // Act
            var result = controller.IsEmailAwaliable("email") as JsonResult;

            // Assert           
            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void IsEmailAwaliableThirdTest()
        {
            User user = new User();
            user.Email = "m";
            userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(user);
            userMock.Setup(a => a.IsEmailExist(It.IsAny<string>())).Returns(false);

            EditController controller = new EditController(userMock.Object, security.Object, image.Object);

            // Act
            var result = controller.IsEmailAwaliable("email") as JsonResult;

            // Assert           
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IsPhoneAwaliableErrorTest()
        {
            EditController controller = new EditController(userMock.Object, security.Object, image.Object);

            // Act
            var result = controller.IsPhoneAwaliable(null) as JsonResult;
        }

        [TestMethod]
        public void IsPhoneAwaliableTest()
        {
            User user = new User();
            user.Phone = "+380992525254";
            userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(user);

            EditController controller = new EditController(userMock.Object, security.Object, image.Object);

            // Act
            var result = controller.IsPhoneAwaliable("+380992525254") as JsonResult;

            // Assert           
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void IsPhoneAwaliableSecondTest()
        {
            User user = new User();
            user.Phone = "14";
            userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(user);
            userMock.Setup(a => a.IsPhoneExist(It.IsAny<string>())).Returns(true);

            EditController controller = new EditController(userMock.Object, security.Object, image.Object);

            // Act
            var result = controller.IsPhoneAwaliable("145285854555") as JsonResult;

            // Assert           
            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void IsPhoneAwaliableThirdTest()
        {
            User user = new User();
            user.Phone = "1";
            userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(user);
            userMock.Setup(a => a.IsPhoneExist(It.IsAny<string>())).Returns(false);

            EditController controller = new EditController(userMock.Object, security.Object, image.Object);

            // Act
            var result = controller.IsPhoneAwaliable("123456789123") as JsonResult;

            // Assert           
            Assert.IsNotNull(result);
        }
    }
}
