﻿
        (function() {
            // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
            if (!String.prototype.trim) {
                (function() {
                    // Make sure we trim BOM and NBSP
                    var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                    String.prototype.trim = function() {
                        return this.replace(rtrim, '');
                    };
                })();
            }

            [].slice.call(document.querySelectorAll('input.input__field')).forEach(function (inputEl) {
                // in case the input is already filled..
                if( inputEl.value.trim() !== '' ) {
                    classie.add( inputEl.parentNode, 'input--filled' );
                }

                // events:
                inputEl.addEventListener('focus', onInputFocus);
                console.log('input add focus');
                inputEl.addEventListener('blur', onInputBlur);
                console.log('input add blur');
            } );

            function onInputFocus( ev ) {
                classie.add( ev.target.parentNode, 'input--filled' );
            }

            function onInputBlur( ev ) {
                if( ev.target.value.trim() === '' ) {
                    classie.remove( ev.target.parentNode, 'input--filled' );
                }
            }
        })();


        (function () {
            // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
            if (!String.prototype.trim) {
                (function () {
                    // Make sure we trim BOM and NBSP
                    var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                    String.prototype.trim = function () {
                        return this.replace(rtrim, '');
                    };
                })();
            }
                       

            [].slice.call(document.querySelectorAll('.calend')).forEach(function (dateEl) {
                // in case the input is already filled..
                if (dateEl.value.trim() !== '') {
                classie.add(dateEl.parentNode, 'input--filled');
                }
                

                // events:
                dateEl.addEventListener('focus', onInputFocus);
                //console.log('input add focus');
                dateEl.addEventListener('blur', onInputBlur);
                //console.log('input add blur');
            });

            function onInputFocus(ev) {
                //if (inputEl.value.trim() === '') {
                //classie.add(inputEl.parentNode, 'input--filled');
                // }
                classie.add(ev.target.parentNode, 'input--filled');
            }

            function onInputBlur(ev) {
                if (ev.target.value.trim() === '') {
                    classie.add(ev.target.parentNode, 'input--filled');
                }
            }
        })();

        (function () {
            // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
            if (!String.prototype.trim) {
                (function () {
                    // Make sure we trim BOM and NBSP
                    var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                    String.prototype.trim = function () {
                        return this.replace(rtrim, '');
                    };
                })();
            }

       
            [].slice.call(document.querySelectorAll('textarea.textarea__field')).forEach(function (textareaEl) {
                // in case the input is already filled..
                if (textareaEl.value.trim() !== '') {
                    classie.add(textareaEl.parentNode, 'textarea--filled');
                }

                console.log(textareaEl.value);
                console.log('textarea in slice');
                // events:
                textareaEl.addEventListener('focus', onInputFocus);
                console.log('textarea add focus');
                textareaEl.addEventListener('blur', onInputBlur);

                console.log('textarea add blur');
            });

            function onInputFocus(ev) {
                classie.add(ev.target.parentNode, 'textarea--filled');
            }

            function onInputBlur(ev) {
                if (ev.target.value.trim() === '') {
                    classie.remove(ev.target.parentNode, 'textarea--filled');
                }
            }
        })();

        (function () {
            // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
            if (!String.prototype.trim) {
                (function () {
                    // Make sure we trim BOM and NBSP
                    var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                    String.prototype.trim = function () {
                        return this.replace(rtrim, '');
                    };
                })();
            }


            [].slice.call(document.querySelectorAll('select.input__field')).forEach(function (selectEl) {
                // in case the input is already filled..
                if (selectEl.value.trim() !== '') {
                    classie.add(selectEl.parentNode, 'input--filled');
                }

                console.log(selectEl.value);
                console.log('textarea in slice');
                // events:
                selectEl.addEventListener('focus', onInputFocus);
                console.log('textarea add focus');
                selectEl.addEventListener('blur', onInputBlur);

                console.log('textarea add blur');
            });

            function onInputFocus(ev) {
                classie.add(ev.target.parentNode, 'input--filled');
            }

            function onInputBlur(ev) {
                if (ev.target.value.trim() === '') {
                    classie.remove(ev.target.parentNode, 'input--filled');
                }
            }
        })();