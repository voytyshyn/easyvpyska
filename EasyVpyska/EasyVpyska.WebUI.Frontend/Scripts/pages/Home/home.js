﻿(function ($) {

    var LayoutPage = function () {

        var that = this;

        this.initialize = function () {
            console.log("[home page] initialize", arguments);

            this.$logOut = $("#logOut");

            this.$logOut.on("click", this.onLogOut);
        };

        this.onLogOut = function () {
            console.log("[home page] onLogOut", arguments, this);
            $.ajax({
                url: "/LogIn/LogOut",
                type: "POST",
                dataType: "json",
            }).done(function (data) {
                console.log("[home page] onLogOut - done", arguments, this);

                if (data.logOutSuccess) {
                    console.log("[home page] onLogOut - logOut success", arguments, this);
                    location.reload(true);
                } else {
                    console.log("[home page] onLogOut - logOut unsuccess", arguments, this);
                }

            });
        };
    };
    $(function () {
        console.log("Home page has loaded");
        var page = new LayoutPage();
        page.initialize();
    });
})(window.jQuery);