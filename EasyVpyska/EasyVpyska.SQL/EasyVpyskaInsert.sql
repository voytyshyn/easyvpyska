﻿USE [EasyVpyska];

GO

SET IDENTITY_INSERT [dbo].[tblRole] ON;

GO

INSERT INTO [dbo].[tblRole]
(
	Id,
	Role,
	Description
)
VALUES 
	(1, 'admin', 'User with permition to administrate other users'),
	(2, 'user', 'User with standart permitions');

GO

SET IDENTITY_INSERT [dbo].[tblRole] OFF;

GO

SET IDENTITY_INSERT [dbo].[tblUser] ON;

GO

INSERT INTO [dbo].[tblUser]
(   
	Id			 ,
	Login        ,
	Password     ,
	Role         ,
	Firstname    ,
	Surname      ,
	DateOfBirth  ,
	Email        ,
	Phone        ,
	Country      ,
	Town         ,
	Address      ,
	Status       ,
	AboutMyself
)
VALUES 
-- $2a$10$9j6V6XeLjcLmYbutCyBn2.o6dGL0IkW6UEy8aZIEjOlbVpg4.68he  ->   321654987
	(1, 'voland',	'$2a$10$9j6V6XeLjcLmYbutCyBn2.o6dGL0IkW6UEy8aZIEjOlbVpg4.68he',     1, N'Stanislav', N'Lem', '1992-06-23', 'voland@gmail.com', '+380974556816', N'Ukraine', N'Lviv', N'Проспект Свободи 33', 1, NULL),

	(2, 'piterpan', '$2a$10$9j6V6XeLjcLmYbutCyBn2.o6dGL0IkW6UEy8aZIEjOlbVpg4.68he',          2, N'Pitya', N'Pan', '1958-01-13', 'voycikhovsky@mail.ru', '+380995156814', N'Ukraine', N'Vinnytsia', N'Наливайченка 3', 1, N'AboutMyself2!'),

	(3, 'vitya',	'$2a$10$9j6V6XeLjcLmYbutCyBn2.o6dGL0IkW6UEy8aZIEjOlbVpg4.68he',       2, N'Vitya', N'Beliyckiy', '1982-11-20', 'viter@gmail.com', '+380974585459', N'Ukraine', N'Kharkiv', N'Промислова 14', 1, NULL),

	(4, 'anya',		'$2a$10$9j6V6XeLjcLmYbutCyBn2.o6dGL0IkW6UEy8aZIEjOlbVpg4.68he',   2, N'Ann', N'Braun', '1973-10-22', 'anyaroz@email.ua', '+380634515817', N'Ukraine', N'Odesa', N'Арсена Люпена 23', 1, N'AboutMyself4'),

	(5, 'vayaki',   '$2a$10$9j6V6XeLjcLmYbutCyBn2.o6dGL0IkW6UEy8aZIEjOlbVpg4.68he',           2, N'Stefan', N'Blender', '1985-09-15', 'vayaki@meta.ua', '+380674511811', N'Ukraine', N'Pryluka', N'Івана Франка 48', 1, N'AboutMyself25'),

	(6, 'moroz',    '$2a$10$9j6V6XeLjcLmYbutCyBn2.o6dGL0IkW6UEy8aZIEjOlbVpg4.68he',          2, N'Arkadiy', N'Moroz', '1953-01-03', 'poltorak@rabmler.ru', '+380544515817', N'Ukraine', N'Zaporizhia', N'Вереса 2', 0, NULL),

	(7, 'nata',     '$2a$10$9j6V6XeLjcLmYbutCyBn2.o6dGL0IkW6UEy8aZIEjOlbVpg4.68he',    2, N'Natali', N'Nadoli', '1989-05-19', 'nata@ukr.net', '+380984515817', N'Ukraine', N'Cherkasy', N'Арсена Люпена 23', 1, N'AboutMyself7.'),

	(8, 'inata',     '$2a$10$9j6V6XeLjcLmYbutCyBn2.o6dGL0IkW6UEy8aZIEjOlbVpg4.68he',    2, N'Inessa', null, '1989-05-19', 'natasha@ukr.net', null, null, null, null, 1, null);
GO

SET IDENTITY_INSERT [dbo].[tblUser] OFF;

GO

SET IDENTITY_INSERT [dbo].tblAd ON;

GO

INSERT INTO [dbo].tblAd
(
	Id			,
	IsRequest	,
	IsEnabled	,
	Title		,
	AuthorId	,
	Description ,
	Begin_date	,
	End_date	,
	Country		,
	Town		,
	IsActive    ,
	Address		
	

)
VALUES 	
	(1, 0, 1, N'Flat', 1, N'Let`s be friends', '2015-07-01', '2015-07-29', N'Ukraine', N'Lviv',1, N'Проспект Свободи 12'),
	(2, 1, 1, N'Room', 3, N'You can write me any time', '2015-07-21', '2015-08-23', N'Ukraine', N'Kharkiv',1, N''),
	(3, 0, 0, N'I have a place', 5, N'Only young guests', '2015-03-01', '2016-01-01', N'Ukraine', N'Kharkiv',0, N'Батуринська 15'),
	(4, 0, 1, N'Can take 1 person', 2, N'Write to me. I prefer younger', '2015-07-21', '2015-07-27', N'Ukraine', N'Kyiv',1, N'Хрещатик 1'),
	(5, 0, 0, N'Hi, everybody', 1, N'Only quite guests', '2015-09-29', '2015-09-30', N'Ukraine', N'Kyiv',1, N''),
	(6, 0, 1, N'Can take 5 person', 6, N'Call me. I prefer workers', '2015-10-01', '2015-12-29', N'Ukraine', N'Lviv',0, N'Зелена 149а'),
	(7, 1, 1, N'Can take on night', 1, N'Only polite guests', '2015-08-01', '2015-08-29', N'Ukraine', N'Lviv',1, N'Проспект Свободи 3'),
	(8, 0, 1, N'Looking for a flat', 3, N'I`m a traveller', '2015-08-21', '2015-09-23', N'Ukraine', N'Kharkiv',1, N''),
	(9, 0, 0, N'Room', 5, N'Ad Description should be here', '2015-03-01', '2016-01-01', N'Ukraine', N'Kharkiv',1, N'Батуринська 1'),
	(10, 1, 1, N'One free Room', 2, N'for 2 young girls', '2015-07-21', '2015-08-27', N'Ukraine', N'Kyiv',1, N'Хрещатик 10,'),
	(11, 1, 0, N'I need a bed', 1, N'I`m a polite guest', '2015-07-29', '2015-08-30', N'Ukraine', N'Kyiv',0, N''),
	(12, 0, 1, N'Friends!', 6, N'Let`s be friends. Give me a home on 3 days', '2015-04-01', '2015-12-29', N'Ukraine', N'Lviv',0, N'Зелена 109'),
	(13, 1, 1, N'Attention!', 1, N'You can write me any time about room ', '2015-08-01', '2015-09-29', N'Ukraine', N'Lviv',1, N'Проспект Свободи 20'),
	(14, 1, 0, N'Partys', 3, N'Let`s be friends', '2015-07-21', '2015-09-23', N'Ukraine', N'Kharkiv',1, N''),
	(15, 0, 0, N'Flat', 5, N'Call me. I prefer artists', '2015-03-01', '2016-01-01', N'Ukraine', N'Kharkiv',1, N'Батуринська 28'),
	(16, 0, 1, N'Just a new title', 2, N'AdDescription16', '2015-10-21', '2015-10-27', N'Ukraine', N'Kyiv',1, N'Хрещатик 9'),
	(17, 1, 1, N'AdName17', 1, N'AdDescription17', '2015-09-29', '2015-10-01', N'Ukraine', N'Kyiv',1, N''),
	(18, 0, 1, N'have place', 1, N'Call me. I prefer travellers', '2015-09-01', '2015-12-29', N'Ukraine', N'Lviv',1, N'Зелена 1'),
	(19, 0, 1, N'free bed', 2, N'You can write me any time', '2015-08-21', '2015-08-27', N'Ukraine', N'Kyiv',0, N'Хрещатик 30'),
	(20, 1, 0, N'for one night', 6, N'Who has a free bed?', '2015-08-29', '2015-08-30', N'Ukraine', N'Kyiv',1, N''),
	(21, 1, 0, N'one week', 1, N'Who has a flat for 2 young girls?', '2015-07-01', '2015-12-29', N'Ukraine', N'Lviv',1, N'Зелена 14'),
	(22, 1, 0, N'Looking for a bed', 1, N'Who has beds for 2 young girls?', '2015-09-01', '2015-09-29', N'Ukraine', N'Lviv',0, N'Проспект Свободи 1'),
	(23, 1, 0, N'Looking for a friend', 3, N'Please, help me with my problem! I need a bed on two nights', '2015-07-21', '2015-07-23', N'Ukraine', N'Kharkiv',0, N''),
	(24, 1, 1, N'Help me', 5, N'I need a bed on one night', '2015-09-01', '2016-01-01', N'Ukraine', N'Kharkiv',1, N'Батуринська 8'),
	(25, 1, 0, N'Hi, everybody', 2, N'I can help owner with cleaning', '2015-07-21', '2015-09-27', N'Ukraine', N'Kyiv',1, N'Хрещатик 13'),
	(26, 1, 0, N'Hi, friends', 1, N'Who has a flat for 2 young girls?', '2015-09-29', '2015-10-30', N'Ukraine', N'Kyiv',1, N''),
	(27, 1, 0, N'I`m a traveller', 6, N'Please, help me! I need a bed on two nights', '2015-10-01', '2015-12-29', N'Ukraine', N'Lviv',1, N'Зелена 22'),
	(28, 1, 1, N'Hi, everybody', 1, N'Who has a room for 5 young girls?', '2015-08-01', '2015-08-29', N'Ukraine', N'Lviv',1, N'Проспект Свободи 5'),
	(29, 1, 1, N'Hot ad', 2, N'I`m a traveller', '2015-08-11', '2015-08-29', N'Ukraine', N'Lviv',1, N'Ф. Листа 3'),
	(30, 1, 1, N'Ad about flat', 1, N'I can help owner with cleaning', '2015-09-01', '2015-09-27', N'Ukraine', N'Lviv',1, N'Проспект Свободи 23'),
	(31, 1, 0, N'Please, anybody', 1, N'Help me with my problem! I need a bed on two nights', '2015-09-01', '2015-10-29', N'Ukraine', N'Lviv',1, N'Валова 3'),
	(32, 1, 0, N'Just one day', 1, N'Please, help me with my problem! I need a bed on one night', '2015-09-01', '2015-11-12', N'Ukraine', N'Lviv',1, N'Валова 12');

SET IDENTITY_INSERT [dbo].tblAd OFF;



SET IDENTITY_INSERT [dbo].tblAdComment ON;

GO

INSERT INTO [dbo].tblAdComment
(
	Id			,
	IsEnabled	,
	AdId		,
	UserId		,
	[Time]		,
	[Comment]	

)
VALUES 
	(1, 1, 1, 3, '2015-04-01', N'Hi! Nice Ad'),
	(2, 1, 1, 1, '2015-04-02', N'It`s interesting'),
	(3, 1, 1, 3, '2015-04-03', N'Cooooooool');

SET IDENTITY_INSERT [dbo].tblAdComment OFF;


SET IDENTITY_INSERT [dbo].tblAdViewers ON;

GO

INSERT INTO [dbo].tblAdViewers
(
	Id			,
	AdId		,
	UserId		

)
VALUES 
	(1, 1, 1),
	(2, 1, 2),
	(3, 1, 3),
	(4, 2, 1),
	(5, 2, 2),
	(6, 2, 4),
	(7, 2, 3),
	(8, 3, 2),
	(9, 3, 3);
SET IDENTITY_INSERT [dbo].tblAdViewers OFF;