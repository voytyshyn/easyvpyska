﻿using EasyVpyska.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using EasyVpyska.WebUI.Abstract;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.Repositories.Sql;

namespace EasyVpyska.WebUI.Frontend.Controllers
{
    
    public class SearchAdController : Controller
    {
        #region Fields
        private readonly IAdRepository _adRepository;
        private readonly IUserRepository _userRepository;
        private readonly IImageService _imageService;
        #endregion

        #region Constructor
        public SearchAdController(IAdRepository adRepository, IUserRepository userRepository,
            IImageService imageService)
        {
            _imageService = imageService;
            _adRepository = adRepository;
            _userRepository = userRepository;
        }
        #endregion

        #region Methods
        //
        // GET: /FindAd/
        [HttpGet]
        public ActionResult Index()
        {
            AdFilter filter = new AdFilter();
            filter.IsEnabled = true;
            filter.IsActive = true;
            filter.Start = DateTime.Now;

            List<Ad> adsList = _adRepository.GetAds(filter, 1, 6);

            for (int i = 0; i < adsList.Count; i++)
            {
                var image = _imageService.LoadPhoto(adsList[i].Author.Id);

                if (image.File != null)
                {
                    var base64Image = "data:image/jpg;base64," + Convert.ToBase64String(image.File);
                    adsList[i].Author.ImageString = base64Image;
                }
                else
                {
                    adsList[i].Author.ImageString = @"/Styles/Image/icon-user.png";
                }
            }

            ViewBag.ListOfAds = adsList;
           
            ViewBag.Count = _adRepository.CountSuitableAds(filter);
               
            return View();
        }


        [HttpPost]
        public PartialViewResult Filter(string typeAd, string city, string ArrivalDate, string leaveDate, int page, int ads)
        {
            bool? typeOfAd;
            DateTime? arriveDt = DateTime.Now;
            DateTime? leaveDt = null;

            if (typeAd.ToLower() == "search")
            {
                typeOfAd = true;

            }
            else if (typeAd.ToLower() == "offer")
            {
                typeOfAd = false;
            }
            else
            {
                typeOfAd = null;            
            }

            if (ArrivalDate != String.Empty)
            {
                 arriveDt = DateTime.ParseExact(ArrivalDate, "dd.MM.yyyy", null);
            }

            if (leaveDate != String.Empty)
            {
                leaveDt = DateTime.ParseExact(leaveDate, "dd.MM.yyyy", null);
            }
            if (city== String.Empty)
            {
                city = null;
            }

            AdFilter filter = new AdFilter();
            filter.IsRequest = typeOfAd;
            filter.IsEnabled = true;
            filter.AuthorId = null;
            filter.Town = city;
            filter.Start = arriveDt;
            filter.Finish = leaveDt;
            filter.IsActive = true;

            List<Ad> adsFilterList = _adRepository.GetAds(filter, page, ads);

            for (int i = 0; i < adsFilterList.Count; i++)
            {
                var image = _imageService.LoadPhoto(adsFilterList[i].Author.Id);

                if (image.File != null)
                {
                    var base64Image = "data:image/jpg;base64," + Convert.ToBase64String(image.File);
                    adsFilterList[i].Author.ImageString = base64Image;
                }
                else
                {
                    adsFilterList[i].Author.ImageString = @"/Styles/Image/icon-user.png";
                }
            }            

            ViewBag.Count = _adRepository.CountSuitableAds(filter);

            ViewBag.ListOfAds = adsFilterList;
            
            return PartialView("PartialSearchResult");
        }
        #endregion

    }
}