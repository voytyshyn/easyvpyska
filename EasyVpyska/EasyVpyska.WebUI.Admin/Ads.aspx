﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAdminPage.Master" AutoEventWireup="true" CodeBehind="Ads.aspx.cs" Inherits="EasyVpyska.WebUI.Admin.Ads" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="pageHead" ContentPlaceHolderID="headTitle" runat="server">
    <h1>Administrate site ads</h1>
    </asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="filter" runat="server">
    <asp:Label ID="AuthorIdLbl" runat="server" Text="Author Id"></asp:Label>
    <asp:TextBox ID="AuthorIdTB" runat="server"></asp:TextBox>
    <asp:Label ID="TownLbl" runat="server" Text="Town"></asp:Label>
    <asp:TextBox ID="TownTB" runat="server"></asp:TextBox>
    <asp:Label ID="Label1" runat="server" Text="Start"></asp:Label>
    <asp:TextBox ID="tbStartDateTime" runat="server" TextMode="Date"></asp:TextBox>
    <asp:Label ID="Label2" runat="server" Text="End"></asp:Label>
    <asp:TextBox ID="tbFinishDateTime" runat="server" TextMode="Date"></asp:TextBox>
    <asp:DropDownList ID="ddlCheckRequest" runat="server">
        <asp:ListItem>Offer</asp:ListItem>
        <asp:ListItem>Request</asp:ListItem>
        <asp:ListItem Selected="True">Anything</asp:ListItem>
    </asp:DropDownList>
    <asp:DropDownList ID="ddlCheckEnabled" runat="server">
        <asp:ListItem>Disabled</asp:ListItem>
        <asp:ListItem>Enabled</asp:ListItem>
        <asp:ListItem Selected="True">Anything</asp:ListItem>
    </asp:DropDownList>
    <asp:Button ID="FilterBtn" runat="server" OnClick="FilterBtn_Click" Text="Search Ads" />
    <div style="height:20px;"> </div>
    <asp:GridView ID="AdsGridView" runat="server" AllowPaging="True" DataSourceID="ODSads" OnRowDeleting="GridView1_RowDeleting1" AutoGenerateSelectButton="True" DataKeyNames="Id" AutoGenerateColumns="False" OnSelectedIndexChanged="AdsGridView_SelectedIndexChanged" OnSelectedIndexChanging="AdsGridView_SelectedIndexChanging" AutoGenerateEditButton="True" AllowCustomPaging="True" OnPageIndexChanging="AdsGridView_PageIndexChanging" OnDataBinding="AdsGridView_DataBinding" OnDataBound="AdsGridView_DataBound" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical">
        <AlternatingRowStyle BackColor="#CCCCCC" />
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" ReadOnly="True" />
            <asp:BoundField DataField="AuthorId" HeaderText="AuthorId" ReadOnly="True" SortExpression="AuthorId" />
            <asp:CheckBoxField DataField="IsRequest" HeaderText="IsRequest" SortExpression="IsRequest" ReadOnly="True" />
            <asp:CheckBoxField DataField="IsEnabled" HeaderText="IsEnabled" SortExpression="IsEnabled" />
            <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" ReadOnly="True" />
            <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" ReadOnly="True" />
            <asp:BoundField DataField="BeginTime" HeaderText="BeginTime" SortExpression="BeginTime" ReadOnly="True" />
            <asp:BoundField DataField="EndTime" HeaderText="EndTime" SortExpression="EndTime" ReadOnly="True" />
            <asp:BoundField DataField="Country" HeaderText="Country" SortExpression="Country" ReadOnly="True" />
            <asp:BoundField DataField="Town" HeaderText="Town" SortExpression="Town" ReadOnly="True" />
            <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" ReadOnly="True" />
            <asp:BoundField DataField="Views" HeaderText="Views" SortExpression="Views" ReadOnly="True" />
        </Columns>
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />
    </asp:GridView>
    <asp:ObjectDataSource 
        ID="ODSads" 
        runat="server" 
        OnObjectCreating="ODSads_ObjectCreating" 
        SelectMethod="GetAdsMod" 
        TypeName="EasyVpyska.Repositories.Sql.AdRepository" 
        DeleteMethod="DeleteAd" 
        MaximumRowsParameterName="MaximumRows" 
        StartRowIndexParameterName="StartRowIndex" 
        UpdateMethod="UpdateAdStatus" 
        OnSelecting="ODSads_Selecting" 
        EnablePaging="True" 
        SelectCountMethod="CountSuitableAdsMod" 
        OnLoad="ODSads_Load">
        <DeleteParameters>
            <asp:ControlParameter ControlID="AdsGridView" Name="Id" PropertyName="SelectedValue" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:Parameter Name="filter" Type="Object" DefaultValue="" />
            <asp:Parameter Name="StartRowIndex" Type="Int32" />
            <asp:Parameter Name="MaximumRows" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Id" Type="Int32" />
            <asp:Parameter Name="IsEnabled" Type="Boolean" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    </asp:Content>
